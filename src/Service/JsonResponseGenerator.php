<?php
/**
 * Created by PhpStorm.
 * User: scahr
 * Date: 31.05.2018
 * Time: 1:19
 */

namespace App\Service;

class JsonResponseGenerator
{
    const RESULT_OK = true;
    const RESULT_ERROR = false;
    const DEFAULT_CODE = 0;

    /**
     * @var bool
     */
    private $result;

    /**
     * @var array
     */
    private $data;

    /**
     * @var string
     */
    private $error;

    /**
     * @var int
     */
    private $code;

    /**
     * @var array
     */
    private $debug;

    public function __construct()
    {
        $this->clear();
    }

    /**
     * @return $this
     */
    public function clear()
    {
        $this->result = false;
        $this->error = '';
        $this->code = JsonResponseGenerator::DEFAULT_CODE;
        $this->data = [];
        $this->debug = [];

        return $this;
    }

    /**
     * @param bool $result
     *
     * @return JsonResponseGenerator
     */
    public function setResult($result)
    {
        $this->result = $result;
        return $this;
    }

    /**
     * @param array $data
     *
     * @return JsonResponseGenerator
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @param string $error
     *
     * @return JsonResponseGenerator
     */
    public function setError($error)
    {
        $this->error = $error;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'result' => $this->result,
            'code' => $this->code,
            'error' => $this->error,
            'data' => $this->data,
            'debug' => $this->debug
        ];
    }

    /**
     * @param int $code
     *
     * @return JsonResponseGenerator
     */
    public function setCode(int $code): JsonResponseGenerator
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @param mixed $debug
     *
     * @return JsonResponseGenerator
     */
    public function addDebug(mixed $debug): JsonResponseGenerator
    {
        $this->debug[] = $debug;
        return $this;
    }

}