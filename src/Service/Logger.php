<?php
/**
 * Created by PhpStorm.
 * User: scahr
 * Date: 29.08.2018
 * Time: 18:31
 */

namespace App\Service;

class Logger
{
    /**
     * @var string
     */
    private $logDir;

    public function __construct($log_dir)
    {
        $this->logDir = $log_dir;
    }

    /**
     * @param mixed $data
     *
     * @return $this
     */
    public function append($data)
    {
        $backtrace = debug_backtrace();

        $c = explode('\\', $backtrace[1]['class']);
        $filename = sprintf("%s_%s", array_pop($c), $backtrace[1]['function']);

        $dump = EntityNormalizer::normalize($data);

        $ts = date('Y-m-d H:i:s', ServerTimeService::getTimestamp()) . PHP_EOL;

        file_put_contents($this->logDir . $filename . '.log',
            $ts . print_r($dump, true) . PHP_EOL . PHP_EOL,
            FILE_APPEND);

        return $this;
    }

    /**
     * @param mixed $data
     * @param null|string $filename
     * @param bool $fileAppend
     *
     * @return $this
     */
    public function log($data, $filename = null, $fileAppend = false)
    {

        $backtrace = debug_backtrace();
        $c = explode('\\', $backtrace[1]['class']);
        $filename = sprintf("%s_%s", array_pop($c), $backtrace[1]['function']);

        $dump = EntityNormalizer::normalize($data);

        $ts = date('Y-m-d H:i:s', ServerTimeService::getTimestamp()) . PHP_EOL;

        file_put_contents($this->logDir . $filename . '.log',
            $ts . print_r($dump, true) . PHP_EOL . PHP_EOL,
            $fileAppend ? FILE_APPEND : 0);

        return $this;
    }

    /**
     * @param mixed $data
     */
    public function logException($data)
    {
        $backtrace = debug_backtrace();

        $filename = $backtrace[1]['function'];

        $dump = EntityNormalizer::normalize($data);

        file_put_contents($this->logDir . $filename . 'Exception.log', print_r($dump, true));
    }
}