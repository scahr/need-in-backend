<?php
/**
 * Created by PhpStorm.
 * User: scahr
 * Date: 10.08.2018
 * Time: 13:38
 */

namespace App\Service;

class ServerTimeService
{
    const ONE_HOUR = 3600;

    /**
     * @param bool $correction
     *
     * @return int
     */
    public static function getTimestamp($correction = false)
    {
        $tm = ($correction) ? time() - self::ONE_HOUR : time();
        return $tm;
    }

    /**
     * @param bool $correction
     *
     * @return int
     */
    public static function getTimestampMillis($correction = false)
    {
        $tm = ($correction) ? time() - self::ONE_HOUR : time();
        return $tm * 1000;
    }

}