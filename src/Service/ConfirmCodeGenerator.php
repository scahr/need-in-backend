<?php
/**
 * Created by PhpStorm.
 * User: scahr
 * Date: 04.06.2018
 * Time: 15:01
 */

namespace App\Service;

class ConfirmCodeGenerator
{
    const DEFAULT_CODE_LENGTH = 5;

    /**
     * @param int $code_length
     *
     * @return null|string
     */
    public static function generate($code_length = self::DEFAULT_CODE_LENGTH): ?string
    {
        return strtoupper(substr(md5(time()), 0, $code_length));
    }
}