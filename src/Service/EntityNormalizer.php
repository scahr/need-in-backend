<?php
/**
 * Created by PhpStorm.
 * User: scahr
 * Date: 17.06.2018
 * Time: 18:25
 */

namespace App\Service;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class EntityNormalizer {
    /**
     * @param $obj
     *
     * @return array|bool|float|int|object|string
     */
    public static function normalize($obj){
        $encoders = [new JsonEncoder()];
        $normalizer = new ObjectNormalizer();
//            $normalizer->setCircularReferenceLimit(2);

        $serializer = new Serializer([$normalizer], $encoders);

        if (method_exists($normalizer, 'setCircularReferenceHandler'))
            $normalizer->setCircularReferenceHandler(function ($object) {
                return $object->getId();
            });

        return $serializer->normalize($obj);
    }
}