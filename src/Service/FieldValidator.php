<?php
/**
 * Created by PhpStorm.
 * User: scahr
 * Date: 29.08.2018
 * Time: 14:28
 */
namespace App\Service;


use App\Exceptions\ParamNotExistsException;

class FieldValidator {
    /**
     * @param array $data
     * @param string $field
     *
     * @throws ParamNotExistsException
     */
    public static function validate($data, $field){
        $isValid = isset($data[$field]) && ($data[$field] !== '');
        if (!$isValid) {
            throw new ParamNotExistsException($field);
        }
    }
}