<?php
/**
 * Created by PhpStorm.
 * User: scahr
 * Date: 27.09.2018
 * Time: 14:19
 */

namespace App\Exception;

class EmailAlreadyExistsException extends \Exception
{
    public const CODE = 1062;

    public function __construct()
    {
        parent::__construct("Такой email уже используется.", self::CODE);
    }
}