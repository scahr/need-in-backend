<?php
/**
 * Created by PhpStorm.
 * User: scahr
 * Date: 27.09.2018
 * Time: 17:11
 */

namespace App\EventListener;

use App\Service\JsonResponseGenerator;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

class AuthenticationFailureListener
{
    /**
     * @param AuthenticationFailureEvent $event
     */
    public function onAuthenticationFailureResponse(AuthenticationFailureEvent $event)
    {
        /* $data = [
             'result' => false,
             'status' => '401 Unauthorized',
             'message' => 'Неверные логин и/или пароль',
         ];*/

        $jsnR = new JsonResponseGenerator();
        $jsnR->setResult(JsonResponseGenerator::RESULT_ERROR)
            ->setCode(401)
            ->setError('Неверные логин и/или пароль');

        $response = new JWTAuthenticationFailureResponse($jsnR->toArray());

        $resp = new JsonResponse($jsnR->toArray());

        $event->setResponse($resp);
    }
}