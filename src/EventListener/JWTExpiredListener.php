<?php
/**
 * Created by PhpStorm.
 * User: scahr
 * Date: 27.09.2018
 * Time: 17:28
 */

namespace App\EventListener;

use App\Service\JsonResponseGenerator;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTExpiredEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

class JWTExpiredListener {
    /**
     * @param JWTExpiredEvent $event
     */
    public function onJWTExpired(JWTExpiredEvent $event)
    {
        /** @var JWTAuthenticationFailureResponse */
//        $response = $event->getResponse();

        $jsnR = new JsonResponseGenerator();
        $jsnR->setResult(JsonResponseGenerator::RESULT_ERROR)
            ->setCode(401)
            ->setError('Срок действия токена истёк');

//        $response = new JWTAuthenticationFailureResponse($jsnR->toArray());

        $resp = new JsonResponse($jsnR->toArray());

        $event->setResponse($resp);

//        $response->setMessage('Your token is expired, please renew it.');
    }
}