<?php

namespace App\Controller;

use App\Entity\Client;
use App\Exception\EmailAlreadyExistsException;
use App\Service\JsonResponseGenerator;
use App\Service\Logger;
use App\Service\ServerTimeService;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ApiAuthController extends AbstractController
{
    /**
     * @var JsonResponseGenerator
     */
    private $jsonResponseGenerator;

    /**
     * @var Logger
     */
    private $logger;

    public function __construct(Logger $logger)
    {
        $this->jsonResponseGenerator = new JsonResponseGenerator();
        $this->logger = $logger;
    }

    /**
     * @Route("/auth", name="apiAuth_auth")
     */
    public function index()
    {
        /*return $this->render('auth/index.html.twig', [
            'controller_name' => 'AuthController',
        ]);*/

        return $this->json($this->jsonResponseGenerator->toArray());
    }

//    public function login

    /**
     * @Route("/register", name="apiAuth_register", host="api.need-in.com")
     * @param Request $request
     *
     * @param UserPasswordEncoderInterface $encoder
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws EmailAlreadyExistsException
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $client = new Client();

            $email = $request->request->get('email');
            $password = $request->request->get('password');
            $tm = ServerTimeService::getTimestamp();
            $client->setCreatedAt($tm)
                ->setUpdatedAt($tm)
                ->setEmail($email)
                ->setPassword($encoder->encodePassword($client, $password));

            $em->persist($client);
            $em->flush();

            $this->logger->log([$request->request->all(), $client]);

            $this->jsonResponseGenerator
                ->setResult(JsonResponseGenerator::RESULT_OK);
        } catch (UniqueConstraintViolationException $e) {
            throw new EmailAlreadyExistsException();
        } catch (\Exception $e) {
            $this->jsonResponseGenerator
                ->setResult(JsonResponseGenerator::RESULT_ERROR)
                ->setError($e->getMessage());
        }

        return $this->json($this->jsonResponseGenerator->toArray());
    }
}
