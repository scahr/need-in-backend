<?php
/**
 * Created by PhpStorm.
 * User: scahr
 * Date: 27.09.2018
 * Time: 13:32
 */

namespace App\Controller;

use App\Service\JsonResponseGenerator;
use App\Service\Logger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ApiController extends AbstractController {
    /**
     * @var JsonResponseGenerator
     */
    private $jsonResponseGenerator;

    /**
     * @var Logger
     */
    private $logger;

    public function __construct(Logger $logger)
    {
        $this->jsonResponseGenerator = new JsonResponseGenerator();
        $this->logger = $logger;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function api(){
        $this->logger->log($this->getUser());

        $this->jsonResponseGenerator->setResult(JsonResponseGenerator::RESULT_OK)
            ->setData([uniqid()]);

        return $this->json($this->jsonResponseGenerator->toArray());
    }
}
