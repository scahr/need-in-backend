<?php
/**
 * Created by PhpStorm.
 * User: scahr
 * Date: 26.07.2018
 * Time: 20:02
 */

namespace App\Controller;

use App\Service\JsonResponseGenerator;
use Symfony\Bundle\TwigBundle\Controller\ExceptionController;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Twig\Environment;

class ApiExceptionController extends ExceptionController
{
    /**
     * @var JsonResponseGenerator
     */
    private $jsonRespGenerator;

    /**
     * ApiExceptionController constructor.
     *
     * @param Environment $twig
     * @param bool $debug
     */
    public function __construct(Environment $twig, $debug)
    {
        parent::__construct($twig, $debug);
        $this->jsonRespGenerator = new JsonResponseGenerator();
    }

    /**
     * @param Request $request
     * @param FlattenException $exception
     * @param DebugLoggerInterface|null $logger
     *
     * @return JsonResponse
     */
    public function showAction(Request $request, FlattenException $exception, DebugLoggerInterface $logger = null)
    {
        $this->jsonRespGenerator->setResult(false)
            ->setCode($exception->getCode())
            ->setError($exception->getMessage());

        return new JsonResponse($this->jsonRespGenerator->toArray());
    }
}